###################
Seefar PDB
###################

PDB is an Application for manage Projects of Seefar, using Codeigniter 3.1.3 , with Charisma Admin Template

Default login :  dedy.adhiewirawan@gmail.com / password.

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.
	- short_open_tag = on
	- enable mysql and mysqli extension
	- Add path C:/php to Environment 
	- install local cert
		download cacert.pem from https://curl.haxx.se/docs/caextract.html
		copy to C:\xampp\php\extras\ssl\cacert.pem
		in your php.ini put this line in this section:
		;;;;;;;;;;;;;;;;;;;;
		; php.ini Options  ;
		;;;;;;;;;;;;;;;;;;;;
		curl.cainfo = "C:\xampp\php\extras\ssl\cacert.pem"
	- enable php_openssl.dll
	- enable php_curl.dll

MYSQL version 5.x

Apache 2.x
	- Enable Mod_rewrite
	- Allow override All
	

*****************
Command line
*****************
To import country 	:  /usr/bin/php index.php import country
To sync wrike folder,folder detail(custom fields, permalink, updatedDate,createdDate, etc), bd folder : 	
	php index.php wrike folder sync
incremental folder sync :
	php index.php wrike folder sync incremental

to sync wrike contacts : 
	php index.php wrike contact sync

to sync wrike customfields : 
	php index.php wrike customfields sync

to sync wrike timelogs : 
	php index.php wrike task syncBDFolderTimeLogs

to sync wrike incremental timelogs : 
	php index.php wrike task syncBDFolderTimeLogs incremental

to sync wrike task : 
	php index.php wrike task syncBDFolderTasks

to sync wrike incremental task : 
	php index.php wrike task syncBDFolderTasks incremental

to sync wrike all , incremental: 
	php index.php wrike sync

Report security issues to `Dedy <mailto:dedy@geraiweb.com>` , thank you.
