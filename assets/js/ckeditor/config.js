/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.enterMode = CKEDITOR.ENTER_BR;
	config.autoParagraph=false;
	config.shiftEnterMode = CKEDITOR.ENTER_P;
	  config.toolbar_Basic =[
	  	['Source','Styles','Format','Font','FontSize','Bold','Italic','Underline','StrikeThrough','BulletedList','Link','Unlink','-','Outdent','Indent','-','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
	  	
    ];
};
