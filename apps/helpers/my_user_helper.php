<?php

function isAdmin(){
	$CI =& get_instance();
	return $CI->ion_auth->is_admin();
}

// user group = 2 is member
function isMember(){
    $CI =& get_instance();
	if ($CI->ion_auth->in_group(2)){
		return true;
    }
    return false;
}

function isManager(){
    $CI =& get_instance();
	if ($CI->ion_auth->in_group(2)){
		return true;
    }
    return false;
}

function _UserId(){
	$CI =& get_instance();
    return $CI->session->userdata('user_id');
}

function isLoggedIn(){
	$CI =& get_instance();
	return $CI->ion_auth->logged_in();
}

function isGoogleSignIn(){
	$CI =& get_instance();
    $google_sign_in =  $CI->session->userdata('google_sign_in');
    if($google_sign_in){
    	return true;
    }else{
    	return false;
    }
}

function _UserName(){
	$CI =& get_instance();
	$user = $CI->ion_auth->user()->row();
	return $user->username;

}


function _Fullname(){
	$CI =& get_instance();
	$user = $CI->ion_auth->user()->row();
	return $user->full_name;

}
?>