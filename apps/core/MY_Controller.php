<?php

class MY_Controller extends MX_Controller{

 	var $viewparams;
 	var $menu,$submenu;
	
	function __construct(){
		
		parent::__construct();
		
		header("Cache-Control: no-cache, no-store, must-revalidate");
    	header("Pragma: no-cache"); 
    	date_default_timezone_set('Asia/Jakarta');

        //-- for debugging
		$this->firephp->setEnabled(config_item('debug'));
        
		$this->viewparams['website_title']   		= config_item('website_title');
        $this->viewparams['website_description']   	= config_item('website_description');
        
        //get from config file
		//default total data per page
		$this->viewparams['rowNum']	= config_item('rowNum');
		
		//option for total data per page 
		$this->viewparams['rowList'] =  config_item('rowList');
		
		//option for grid height
		$this->viewparams['rowHeight'] =  config_item('rowHeight');
		
	
    	//option for grid width
		$this->viewparams['rowWidth'] =  config_item('rowWidth');
	}
	
    function set_menu($menu,$submenu){
		$this->menu=$menu;
		$this->submenu= $submenu;
	}

    /* backend view */
    function viewpage($page){
    	$menu_data_active = "";
    	$menu_referensi_active = "";

    	$menu_di_active = $menu_kab_active = $menu_iksi_active = "";
    	switch($this->menu){
    		case "data": 
    			$menu_data_active = "active";
    		break;
    		case "referensi":
    			$menu_referensi_active = "active";
    		break;
    	}

    	switch($this->submenu){
    		case "daerah_irigasi" :
    			$menu_di_active = "active";
	    	break;
	    	case "kabupaten" :
	    		$menu_kab_active = "active";
	    	break;
	    	case "iksi" :
	    		$menu_iksi_active = "active";
	    	break;

    	}

    	//set Active/Inactive for menus
		$this->viewparams['menu_data_active'] 		= $menu_data_active;
		$this->viewparams['menu_referensi_active'] 		= $menu_referensi_active;
		$this->viewparams['menu_di_active'] 		= $menu_di_active;
		$this->viewparams['menu_kab_active'] 		= $menu_kab_active;
		$this->viewparams['menu_iksi_active'] 		= $menu_iksi_active;
		
        $this->viewparams['header'] 		= $this->load->view("vheader", $this->viewparams, true);		
		$this->viewparams['menunavigation'] = $this->load->view("vmenu", $this->viewparams, true);		
		$this->viewparams['content'] 		= $this->load->view($page, $this->viewparams, true);
		$this->load->view("vtemplate", $this->viewparams);	
	
	}
	
	/* backend ajax */
	function viewajax($page){
   		$this->load->view($page, $this->viewparams);	
	}
	
	
	/* backend popup view */
	function viewpopup($page){
		$this->viewparams['content'] 	= $this->load->view($page, $this->viewparams,true);
		$this->load->view("vtemplate_popup", $this->viewparams);	
	}
	
	function loadDataJqGrid(){
		
		$page = isset($this->DataJqGrid['page'])?$this->DataJqGrid['page']:1; // get the requested page
        $limit = isset($this->DataJqGrid['limit'])?$this->DataJqGrid['limit']:config_item('rowNum'); // get how many rows we want to have into the grid
        $sidx = isset($this->DataJqGrid['sidx'])?$this->DataJqGrid['page']:''; // get index row - i.e. user click to sort
        $sord = isset($this->DataJqGrid['sord'])?$this->DataJqGrid['sord']:''; // get the direction
 
        if(!$sidx) $sidx =1;
        
       	$query = $this->DataJqGrid['query'];
       	$count = $this->DataJqGrid['count'];
       	
       	if($limit > 0){
	        if( $count > 0 ) {
	            $total_pages = ceil($count/$limit);    //calculating total number of pages
	        } else {
	            $total_pages = 0;
	        }
	    }else{
	    	$total_pages = 1;
	    }
	    
        if ($page > $total_pages) $page=$total_pages;
 
        $start = $limit*$page - $limit; // do not put $limit*($page - 1)
        $start = ($start<0)?0:$start;  // make sure that $start is not a negative value
 		
 		$responce = new stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;
        $i=0;
        $offset = ($page-1)*$limit;
        
        $id = $this->DataJqGrid['id'];
        
        foreach($query as $row) {
            $responce->rows[$i]['id']=$row->$id;
            
            $responce->rows[$i]['cell'][0] = $offset+$i+1;
            $j=1;
            foreach($this->DataJqGrid['column'] as $rowcols){
            	$responce->rows[$i]['cell'][$j] = $row->$rowcols;
            	$j++;
            }
            $i++;
        }
        echo json_encode($responce);
    }
    
	
}

?>
