
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?=$menu_referensi_active?> treeview">
    <a href="#"> <i class="fa fa-laptop"></i>
       <span>Referensi</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class=""><a href="<?=config_item('iidw_url')?>/tab_propinsilist.php"><i class="fa fa-circle-o"></i>Kewenangan</a></li>
      <li><a href="<?=config_item('iidw_url')?>/tab_penilaianlist.php"><i class="fa fa-circle-o"></i>Penilaian Kerja</a></li>
      <li><a href="<?=config_item('iidw_url')?>/tab_bobot_kinerjalist.php"><i class="fa fa-circle-o"></i>Bobot Kinerja</a></li>
    </ul>
  </li>
   <li class="<?=$menu_data_active?> treeview">
    <a href="#"> <i class="fa fa-laptop"></i>
     <span>Data</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li  class='<?=$menu_kab_active?>' ><a href="<?=site_url('kabupaten')?>"><i class="fa fa-circle-o"></i>Kabupaten</a></li>
      <li><a href="<?=config_item('iidw_url')?>/irigasilist.php""><i class="fa fa-circle-o"></i>Daerah Irigasi</a></li>
      <li class='<?=$menu_di_active?>' ><a href="<?=site_url('irigasi')?>"><i class="fa fa-circle-o"></i>Daerah Irigasi</a></li>
    </ul>
  </li>
  <li class='<?=$menu_iksi_active?>' ><a href="<?=site_url('iksi')?>"><i class="fa fa-bar-chart"></i>Index kinerja IKSI</a></li>


 </ul>
   