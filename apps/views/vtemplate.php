<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=config_item('website_title')?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=assets_url()?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=assets_url()?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=assets_url()?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=assets_url()?>dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=assets_url()?>dist/css/skins/_all-skins.min.css">
 
<!-- jQuery 3 -->
<script src="<?=assets_url()?>bower_components/jquery/dist/jquery.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script>
    $(document).ready(function(){
           
        $.ajaxSetup({
            data: {
                csrfpdbtoken : $.cookie('csrfpdbcookie')
            }
        });

        $(document).ajaxError(function() {
            alert('<?=lang('lerror_ajax')?>\r\n<?=lang('lerror_ajax2')?>');
            //window.location.reload(true);
        });

         $(document).ajaxStart(function() {
             $("#ajaxStatusDisplay_userMessage").show();
             
        });

        $( document ).ajaxStop(function() {
             $("#ajaxStatusDisplay_userMessage").hide();
        });
        
    });
    </script>   
    <style type="text/css">
    .ajaxStatusDisplay_userStyle {
        display: none; 
        z-index: 9999;
        position:fixed;
        left:45%;top:0px;height:20;
        background-color:#F2F760;
        color:black;
        padding-left: 5px;
        padding-right: 5px;
        width:75px;
        font-weight: normal;
        font-family:Arial, Helvetica, sans-serif;
      
    }

 /*body {
    padding-top: 75px;
  }*/
    </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	

<div class="wrapper">
<div id="ajaxStatusDisplay_userMessage" class='ajaxStatusDisplay_userStyle'>
    Loading...
    </div>
	<header class="main-header">
		<?=$header?>
	</header>

  	<!-- Left side column. contains the logo and sidebar -->
  	<aside class="main-sidebar">
    	<!-- sidebar: style can be found in sidebar.less -->
    	<section class="sidebar">
		 	<?=$menunavigation?>
		</section>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<?=$content?>

   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!--<b>Version</b> 2.4.0-->
    </div>
    <strong>Copyright &copy; 2017 Ditjen SDA Kementerian PUPR.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->


<!-- jQuery UI 1.11.4 -->
<script src="<?=assets_url()?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=assets_url()?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?=assets_url()?>js/jquery.cookie.js"></script>

<!-- AdminLTE App -->
<script src="<?=assets_url()?>dist/js/adminlte.min.js"></script>

</Body>
</html>
