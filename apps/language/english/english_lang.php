<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['ltahun']	= "Tahun";
$lang['lkinerja_irigasi']	= "Kinerja Sistem Irigasi";
$lang['lalamat']	= "Alamat";
$lang['ltelepon']	= "Telepon";
$lang['lkode_kabupaten']	= "Kode Kabupaten";
$lang['lprovince']	= "Propinsi";
$lang['lluas']		= "Luas (Ha)";
$lang['lnama_irigasi']		= "Nama DI";
$lang['lkabupaten']			= "Kabupaten";
$lang['lkode_irigasi']			= "Kode Irigasi";
$lang['lirigasi']			= "Daerah Irigasi";
$lang['lwrike_folder']			= "Wrike Folder";
$lang['lforbidden_word_exists'] = "You should remove the Pro from the start of the tracking code";
$lang['lerror_ajax']			= "There is something wrong with the system.";
$lang['lerror_ajax2']			= "If the problem persists, please contact administrator.";

$lang['lsearch_add_new']		= "Search/Add New";
$lang['lreset']		 			= "Reset";
$lang['lmaster_data']		 	= "Master Data";
$lang['lconfirm_delete']		= "Are you sure you want to delete this data?";
$lang['ldelete_donor']			= "Delete Donor";
$lang['lmodify']				= "Modify";
$lang['ldonor_name']			= "Name";
$lang['ltracking_code_exists']	= "Tracking code exists";
$lang['lplease_select_donor']	= "Please select donor";
$lang['ldonor_level_2']			= "Donor Level 2";
$lang['ldonor_level_3']			= "Donor Level 3";
$lang['llevel_2'] 				= "Level 2";
$lang['llevel_3'] 				= "Level 3";
$lang['lchange_status_confirmation'] = "Change status confirmation";
$lang['ldata_set_active'] 		= "Data set to active";
$lang['ldata_set_inactive'] 	= "Data set to inactive";
$lang['ldata_set_locked'] 		= "Data set to locked";
$lang['ldata_set_unlocked'] 	= "Data set to unlocked";
$lang['lno_data_on_database'] 	= "No data on database";
$lang['ldata_success_inserted'] = "Data successfully inserted";
$lang['ldata_failed_inserted'] 	= "Data failed to insert";
$lang['ldata_success_updated'] 	= "Data successfully updated";
$lang['ldata_failed_updated'] 	= "Data update failed";
$lang['lalias_exists'] 			= "Alias already exists";
$lang['lname_exists'] 			= "Name already exists";
$lang['ldata_is_deleted'] 		= "Data successfully deleted";
$lang['ldata_is_failed_deleted'] = "Data failed to delete";
$lang['ldelete_confirmation'] 	= "Are you sure you want to permanently delete this data ? ";
$lang['lplease_select_group'] 	= "Please select a user group";
$lang['lplease_select_region'] 	= "Please select a region";
$lang['luser_does_not_exist'] 	= "User does not exist";
$lang['linvalid_token'] 		= "Invalid token";
$lang['lused_in_another_data'] 	= "Used in another data";
$lang['lwill_active'] 		= "will set to active ?";
$lang['lwill_inactive'] 	= "will set to inactive ?";
$lang['lhas_active'] 		= "has been set to active";
$lang['lhas_inactive'] 		= "has been set to inactive";
$lang['lwill_lock'] 		= "will set to lock ?";
$lang['lwill_unlock'] 		= "will set to unlock ?";
$lang['lhas_locked'] 		= "has been set to locked";
$lang['lhas_unlocked'] 		= "has been set to unlocked";

$lang['ltheme_s']	= "Theme(s)";
$lang['ldonor_s']	= "Donor(s)";
$lang['lplease_save_the_project_first']	= "To add donor(s), please save the project first";
$lang['lplease_select_status']	= "Please select status";
$lang['lplease_select_service']	= "Please select service";
$lang['lplease_select_themes']	= "Please select theme";
$lang['lplease_select_country']	= "Please select country";
$lang['lfunding_source']		= "Funding Source";
$lang['lplease_select_funding_source']		= "Please select funding source";
$lang['lbudget_us']				= "Income (USD)";
$lang['lplease_select_owner']	= "Please select owner";
$lang['lprogram_owner']			= "Program Owner";
$lang['lplease_select_project_manager']	= "Please select project manager";
$lang['lplease_select_program']	= "Please select program";
$lang['lproject_detail']		= "Project Detail";
$lang['lmissing_parameter']		= "Missing parameter";
$lang['linvalid_permission']	= "You have invalid permission";
$lang['lproject_is_not_exists'] = "Project does not exists";
$lang['lprogram_name']			= "Program Name";
$lang['lproject_name']			= "Project Name";
$lang['lall_project_manager']	= "All Project Managers";
$lang['lall_status']			= "All Statuses";
$lang['lall_donor']			= "All Donors";
$lang['lall_theme']			= "All Themes";
$lang['lall_service']		= "All Services";
$lang['lall_program'] 		= "All Programs";
$lang['lservice_s']			= "Service(s)";
$lang['ltracking_code']		= "Tracking Code";
$lang['lproject_list']		= "Project List";
$lang['lemail_content']		= "Email Content";
$lang['lerror_message']		= "Error Message";
$lang['lstatus']	= "Status";
$lang['lbcc']		= "Bcc";
$lang['lcc']		= "Cc";
$lang['lfrom']		= "From";
$lang['lview']		= "View";
$lang['lcontent']	= "Content";
$lang['lsubject']	= "Subject";
$lang['lmodify_email_template']	= "Modify Email Template";
$lang['lupdated_by']			= "Updated By";
$lang['lupdated_on']			= "Updated On";
$lang['lcreated_by']			= "Created By";
$lang['lcreated_on']			= "Created On";
$lang['lemail_template']		= "Email Template";
$lang['llist_email']			= "Email List";
$lang['lemail_log']				= "Email Log";
$lang['lplease_enter_your_email']	= "Please enter your email";
$lang['ltoo_many_login_attempts'] 	= "Too many login attempts";
$lang['lset_unlocked']			= "Set to Unlocked";
$lang['lset_locked']			= "Set to Locked";
$lang['lemail_exists']			= "Email address already exists";
$lang['lproject_manager']		= "Project Manager";
$lang['lhome']			= "Home";
$lang['lcountry']		= "Country";
$lang['lregion']		= "Region";
$lang['lprogram']		= "Program";
$lang['ladd_program'] 	= "Add new program";
$lang['lprogram_list']		= "Program List";
$lang['lmodify_program'] 	= "Modify program";
$lang['ladd_region'] 		= "Add new region";
$lang['lregion_list']		= "Region List";
$lang['lall_region'] 		= "All Regions";
$lang['luser_list'] 		= "User List";
$lang['lmodify_region'] 	= "Modify region";
$lang['ladd_country'] 		= "Add new country";
$lang['lcountry_list']		= "Country List";
$lang['lmodify_country'] 	= "Modify country";
$lang['ladd_new'] 			= "Add New";
$lang['linactive'] 			= "Inactive";
$lang['lall'] 				= "All";
$lang['ladd_user'] 			= "Add User";
$lang['lretype_password'] 	= "Retype Password";
$lang['lgroup'] 			= "Group";
$lang['lmodify_user'] 		= "Modify user";
$lang['lok'] 		= "OK";
$lang['lsearch'] 	= "Search";
$lang['lskill']		= "Skill";
$lang['ltheme']		= "Theme";
$lang['ldonor']		= "Donor";
$lang['ladd_theme'] = "Add Theme";
$lang['lmodify_theme'] 		= "Modify Theme";
$lang['lservice']			= "Service";
$lang['lproject']			= "Project";
$lang['ladd_project']		= "Add Project";
$lang['lmodify_project']	= "Modify Project";
$lang['lstart_date']		= "Start Date";
$lang['lend_date']			= "End Date";
$lang['lfull_name'] 	= "Full name";
$lang['llocked'] 		= "Locked";
$lang['ledit'] 			= "Edit";
$lang['ldel'] 			= "Del";
$lang['ltheme_list'] 	= "Theme List";
$lang['ladd_service'] 	= "Add Service";
$lang['lmodify_service'] = "Modify Service";
$lang['lservice_list'] 	= "Service List";
$lang['ldescription']	= "Description";

$lang['ldate']		= "Date";         
$lang['lyes']		= "Yes";
$lang['lno']		= "No";
$lang['lactive']			= "Active";     
$lang['lto']				= "To";
$lang['lexport_confirm']	= "Are you sure you want to export this data?";
$lang['lsubmit']			= "Submit";
$lang['lexport']			= "Export";
$lang['llast_login'] 	= "Last Login";
$lang['lemail']			= "Email";
$lang['luser']			= "User";
$lang['lusername']		= "Username";
$lang['lname'] 			= "Name";
$lang['ldelete'] 		= "Delete";
$lang['lalias'] 		= "Alias";
$lang['lplease_enter_new_password'] 	= "Please enter your new password";
$lang['lchange_password'] 	= "Change Password";
$lang['ladd_donor'] 		= "Add Donor";
$lang['lmodify_donor'] 		= "Modify Donor";
$lang['lconfirm_logout'] 	= "Are you sure you want to logout?";
$lang['llogout'] 			= "Logout";
$lang['login_username_label'] = "Username";
$lang['lpassword'] 			= "Password";
$lang['lcancel'] 			= "Cancel";
$lang['ldonor_list'] 		= "Donor list";
$lang['ldashboard'] 		= "Dashboard";
$lang['ltitle'] 			= "Title";
$lang['lplease_enter_your_username']    = "Please enter your username";
$lang['forgot_password_username_label'] = "Username";
$lang['linvalid_forgot_password_code']  = "Your password code is invalid";
$lang['lusername_not_found']			= "User name not found";
$lang['lforgot_password_message']   	= "A link to reset the password has been sent to your email account";
$lang['lset_active'] 	= "Set Active";
$lang['lset_inactive'] 	= "Set Inactive";

?>
