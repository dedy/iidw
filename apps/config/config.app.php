<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['iidw_url']			= "http://localhost/iidw";
$config['website_title']			= "IIDW- Ditjen SDA Kementrian PUPR";
$config['website_description']		= "Website Description";

/* grid option */
$config['rowNum']		= 25; //number of row per page
$config['rowList']		= "25,50,75"; //option for row per page
$config['rowHeight']	= 480;
$config['rowWidth']	= 1024;

//ckeditor config
$config['ck_height']    = 500;
$config['ck_width']     = 1024;
$config['message_delay']     = 2500;

$config['status_cfg'] = array(
	array("id"=>1	,"name"=>"Pending")
	,array("id"=>2	,"name"=>"Implementing")
	,array("id"=>3	,"name"=>"Completed")
);


$config['funding_source'] = array(
	array("id"=>1	,"name"=>"Externally funded")
	,array("id"=>2	,"name"=>"Internally funded")
);

$config['jqgrid_width'] = 1250;
$config['jqgrid_height'] = 380;

$config['iksi_component_u'] = array(
	array("name"=>"PRASARANA FISIK","index"=>45)
	,array("name"=>"PRODUKTIVITAS TANAM","index"=>15)
	,array("name"=>"SARANA PENUNJANG","index"=>10)
	,array("name"=>"ORGANISASI PERSONALIA","index"=>15)
	,array("name"=>"DOKUMENTASI","index"=>5)
	,array("name"=>"GP3A/IP3A","index"=>10)
);


$config['iksi_component_t'] = array(
	array("name"=>"PRASARANA FISIK","index"=>25)
	,array("name"=>"PRODUKTIVITAS TANAM","index"=>15)
	,array("name"=>"KONDISI OP","index"=>20)
	,array("name"=>"ORGANISASI PERSONALIA","index"=>15)
	,array("name"=>"DOKUMENTASI","index"=>5)
	,array("name"=>"P3A","index"=>20)
);

$config['bobot_utama'] = 0.5;
$config['bobot_tersier'] = 0.5;
