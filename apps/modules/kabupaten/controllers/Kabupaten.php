<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kabupaten extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
        $this->load->model("kabupaten_model");
		$this->load->model("province/province_model");
	}

	public function index() {
        parent::set_menu("data","kabupaten");
         //get list of province
        
        $provinces = $this->province_model->search(0,1,'n_propinsi','asc');

        $this->viewparams['provinces'] = $provinces;
        $this->viewparams['title_page']	= lang('lkabupaten');
        parent::viewpage("vlist");
	}

    public function loadDataGrid() {
        $page = isset($_POST['page'])?$_POST['page']:1;
        $sidx = isset($_POST['sidx'])?$_POST['sidx']:'n_propinsi, n_kabupaten'; // get index row - i.e
        $sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
        $limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid


        $params = array(
            "id_propinsi"   => $this->input->get('id_propinsi'),
            "n_kabupaten"   => $this->input->get('n_kabupaten')
            );

        $query = $this->kabupaten_model->search($limit,$page,$sidx,$sord,$params);
        $this->firephp->log($this->db->last_query());
        $count = $this->kabupaten_model->count_search($params);
        $this->firephp->log($this->db->last_query());
        $this->firephp->log("count : ".$count);
             
        $this->DataJqGrid = array(
            "page"		=> $page,
            "sidx"		=> $sidx,
            "sord"		=> $sord,
            "query" 	=> $query,
            "limit"		=> $limit,
            "count"		=> $count,
            "column"	=> array("id_kabupaten","k_kabupaten","n_propinsi","n_kabupaten","alamat","telepon"),
            "id"		=> "id_kabupaten"
        );
        parent::loadDataJqGrid();

    }

    public function get_active(){
        $id = $this->input->get('id');
        $record = $this->region_model->get_row(array('id' => $id));
        if($record){
            $data['active'] = $record->active;
            echo json_encode($data);
        }
    }

    public function set_active($id){
        $record = $this->region_model->get_row(array('id' =>$id));
        if($record){
            $active = $record->active;
            if($active == '1')
                $data['active'] = '0';
            else
                $data['active'] = '1';
            $this->region_model->update($data,$id);
            $message = ($data['active'] == '1')?lang('ldata_set_active'):lang('ldata_set_inactive');
        } else {
            $message = lang('lno_data_on_database');
        }

        $result = array("message" => $message);
        echo json_encode($result);
    }

    public function add(){
        $this->viewparams['title_page']	= lang('ladd_region');
        $this->form(0);
    }

    public function edit($id){
        $data = $this->region_model->get_row(array('id' =>$id));
        if(!$data){
            redirect('region');
        }

        $this->viewparams['title_page']	= lang('lmodify_region');
        $this->viewparams['data'] = $data;
        $this->form($id);
    }

    public function form($id=0){
        parent::viewpage("vform");
    }

}
