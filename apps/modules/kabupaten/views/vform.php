<script src="<?php echo assets_url(); ?>/js/jquery.form.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){

        $('#cancel-btn').click(function(e){
            e.preventDefault();
            window.location.replace("<?=site_url('region');?>");
        });

        $('#submit-btn').click(function(e){
            //$('form#login-form').submit();
            //return;
            e.preventDefault();
            dopost($(this));
        });

       
    });

    function dopost(obj){
        obj.attr('disabled','disabled');
        $('#loadingmessage').show();

        $.post('<?=site_url('region/do_update');?>',
            $("#input-form").serialize(),
            function(returData) {

                $('#loadingmessage').hide();
                obj.removeAttr('disabled');

                $('#show_message').slideUp('normal',function(){

                    if(returData.error){
                        var rv = '<div class="alert alert-danger">'+returData.message+'</div>';
                        $('#show_message').html(rv);
                        $('#show_message').slideDown('normal');

                    }else{
                        var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                        $('#show_message').html(rv);
                        $('#show_message').slideDown('normal',function(){
                            setTimeout(function() {
                                $('#show_message').slideUp('normal',function(){
                                    if(returData.redirect){
                                        window.location.replace(returData.redirect);
                                    }
                                });
                            }, <?=config_item('message_delay')?>);
                        });
                    }
                });

            },'json');
    }
</script>

<div>
    <ul class="breadcrumb">
        <li> <a href="<?=site_url('/')?>"><?=lang('lhome')?></a></li>
        <li> <a href="<?=site_url('region')?>"><?=lang('lregion')?></a></li>
        <li><a href="#"><?=$title_page?></a></li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <?=$title_page?></h2>
            </div>

            <div class="box-content">
                <?php $hidden = array("id" => (isset($data->id))? $data->id:0); ?>
                <?php echo form_open("region/do_update",array('role'=>"form",'id'=>'input-form' ,'class'=>'form-horizontal'),$hidden)?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><?=lang('lname')?> <span class="required">* </span></label>
                            <div class="col-md-8">
                                <input type="text" id="name" name="name" data-required="1"  class="form-control" maxlength="100" value="<?php if(isset($data->name) && $data->name) echo $data->name ?>"/>
                            </div>
                        </div>


                    </div> <!-- /end col-md-6 -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><?= lang('lactive') ?><span class="required"> *</span></label>
                            <div class="col-md-8">
                                <label class="radio-inline">
                                    <input type="radio" name="active" id="active"
                                           value="1" <?php if ((isset($data->active) && ($data->active == "1")) || !isset($data->active)) {
					                    echo "checked";
				                    } ?>> <?= lang('lyes') ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="active" id="active"
                                           value="0" <?php if ((isset($data->active) && ($data->active == "0"))) {
					                    echo "checked";
				                    } ?>><?= lang('lno') ?>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div id='show_message' style="display: none;"></div>
                    </div>
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsubmit')?></button>
                        <button type="button" class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
                    </div>
                </div>

                <?=form_close();?>

            </div><!-- end box-content -->

        </div><!-- /end box-inner -->
    </div>
</div>
