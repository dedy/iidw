<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iksi extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
        $this->load->model("iksi_model");
	}

	public function index() {
        parent::set_menu("","iksi");
        $this->viewparams['title_page']	= lang('lkinerja_irigasi');
        parent::viewpage("vlist");
	}

    public function loadDataGrid() {
        $page = isset($_POST['page'])?$_POST['page']:1;
        $sidx = isset($_POST['sidx'])?$_POST['sidx']:'tahun,k_di'; // get index row - i.e
        $sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
        $limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid

        $params = array();

        $query = $this->iksi_model->search($limit,$page,$sidx,$sord,$params);
        $this->firephp->log($this->db->last_query());
        $count = $this->iksi_model->count_search($params);
        $this->firephp->log($this->db->last_query());
        $this->firephp->log("count : ".$count);
       if($count){
        for($i =0;$i<$count;$i++){
    $query[$i]->n_di = "<u><a href ='".site_url('iksi/view')."/".$query[$i]->id."'>".$query[$i]->n_di."</a></u>";
        }
       }
        $this->DataJqGrid = array(
            "page"		=> $page,
            "sidx"		=> $sidx,
            "sord"		=> $sord,
            "query" 	=> $query,
            "limit"		=> $limit,
            "count"		=> $count,
            "column"	=> array("id","k_di","n_di","luas","n_propinsi","n_kabupaten","tahun","u1","u2","u3","u4","u5","u6","total_u","t1","t2","t3","t4","t5","t6","total_t"),
            "id"		=> "id_irigasi"
        );
        parent::loadDataJqGrid();

    }

    public function view($id){
        parent::set_menu("","iksi");
        
        //get info detail
        $data = $this->iksi_model->search(1,1,'n_di','asc',array('id'=>$id));
        $this->viewparams['data'] = $data[0];
        $this->viewparams['title_page'] = lang('lkinerja_irigasi');

        $this->viewparams['u_components'] = config_item('iksi_component_u');
        $this->viewparams['t_components'] = config_item('iksi_component_t');
        $this->viewparams['bobot_u'] = config_item('bobot_utama');
        $this->viewparams['bobot_t'] = config_item('bobot_tersier');

        parent::viewpage("vview");
    }

 
}
