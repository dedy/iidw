<script src="<?php echo assets_url(); ?>js/jquery.form.js" type="text/javascript"></script>
<link href='<?php echo assets_url(); ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo assets_url(); ?>js/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo assets_url(); ?>js/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo assets_url(); ?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<!-- jQuery UI -->
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.core.min.js"></script>
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.datepicker.min.js"></script>
<!--jqconfirm -->
<script src="<?php echo assets_url(); ?>js/jquery.confirm.min.js"></script>
<style type="text/css">
    .ui-jqgrid tr.jqgrow td {vertical-align:top !important}
</style>
<script type="text/javascript">
    jQuery().ready(function (){

        $("#search-btn").click(function(e){
            e.preventDefault();
            gridReload();
        });
           
        jQuery("#list1").jqGrid({
            url:'<?=base_url()?>iksi/loadDataGrid',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','<?=lang('lkode_irigasi')?>','<?=lang('lnama_irigasi')?>','<?=lang('lluas')?>','<?=lang('lprovince')?>','<?=lang('lkabupaten')?>','<?=lang('ltahun')?>'
              ,'u1','u2','u3','u4','u5','u6','Total U','t1','t2','t3','t4','t5','t6','Total T'
            ],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:15, align:"right",sortable:false},
                {name:'id',index:'id', hidden: true},
                {name:'code',index:'k_di',width:25,align:"left",stype:'text'},
                {name:'name',index:'n_di',width:35,align:"left",stype:'text'},
                {name:'luas',index:'luas',width:20,align:"left",stype:'text'},
                {name:'name_propinsi',index:'n_propinsi',width:45,align:"left",stype:'text'},
                {name:'name_kabupaten',index:'n_kabupaten',width:45,align:"left",stype:'text'},
                {name:'tahun',index:'tahun',width:15,align:"left",stype:'text'},
                {name:'u1',index:'u1',width:10,align:"left",stype:'text'},
                {name:'u2',index:'u2',width:10,align:"left",stype:'text'},
                {name:'u3',index:'u3',width:10,align:"left",stype:'text'},
                {name:'u4',index:'u4',width:10,align:"left",stype:'text'},
                {name:'u5',index:'u5',width:10,align:"left",stype:'text'},
                {name:'u6',index:'u6',width:10,align:"left",stype:'text'},
                {name:'total_u',index:'total_u',width:10,align:"left",stype:'text'},
                {name:'t1',index:'t1',width:10,align:"left",stype:'text'},
                {name:'t2',index:'t2',width:10,align:"left",stype:'text'},
                {name:'t3',index:'t3',width:10,align:"left",stype:'text'},
                {name:'t4',index:'t4',width:10,align:"left",stype:'text'},
                {name:'t5',index:'t5',width:10,align:"left",stype:'text'},
                {name:'t6',index:'t6',width:10,align:"left",stype:'text'},
                {name:'total_t',index:'total_t',width:10,align:"left",stype:'text'},
          
                
            ],
            rowNum:<?=$rowNum?>,
            <?php if(isset($rowList) && $rowList){ ?>
            rowList:[<?=$rowList?>],
            <?php }?>
               width: null,
        //    fixwidth: false,
            autowidth: true,
            height: <?=config_item('jqgrid_height')?>,
          pager: '#pager1',
            viewrecords: true,
            caption:"<?=lang('lirigasi')?>",
            sortname: 'n_kabupaten',
            toppager: true,
            
            loadComplete: function(data) {
                $("#total_records").html("("+data.records+")");
            }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});

    });


    function gridReload() {
        var n_kabupaten =(jQuery("#n_kabupaten").val())?jQuery("#n_kabupaten").val():'';
        var n_irigasi =(jQuery("#n_irigasi").val())?jQuery("#n_irigasi").val():'';
        var id_propinsi =(jQuery("#id_propinsi").val())?jQuery("#id_propinsi").val():'';
   
        jQuery("#list1").jqGrid('setGridParam',{
            url:"<?=site_url('iksi/loadDataGrid')?>/?n_irigasi="+n_irigasi+"&n_kabupaten="+n_kabupaten+"&id_propinsi="+id_propinsi,
            page:1
        }).trigger("reloadGrid");
        
    }


</script>

 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title_page?>
      </h1>

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><?=$title_page?></a></li>

      </ol>
    </section>

    <!-- Main content -->
     <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
            <div class="row">
              <div class="col-md-12">
                  <div class="col-md-12" style="margin-top:15px;">
                      <div id='show_message' style="display: none;"></div>
                      <table id="list1"></table> <!--Grid table-->
                      <div id="pager1"></div>  <!--pagination div-->
                             
                  </div>
              </div>
        </div>
    </section>
       