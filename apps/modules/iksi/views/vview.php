  <!-- Morris charts -->
  <link rel="stylesheet" href="<?=assets_url()?>bower_components/morris.js/morris.css">
<style>
.donut-chart text {
  fill: white;

}
</style>
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title_page?>
      </h1>

      <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?=site_url('iksi')?>"><?=lang('lkinerja_irigasi')?></a></li>
        <li class="active"><?=$title_page?></a></li>

      </ol>
    </section>

    <!-- Main content -->
     <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12" style="margin-top:15px">
            <strong><font style='font-size:18px'><?=$data->k_di?> / <?=$data->n_di?> / <?=$data->n_kabupaten?> / <?=$data->n_propinsi?> / <?=$data->luas?> Ha</font></strong>
            </div>
          </div>
          <div class='row'>

            <div class="col-md-12" style="margin-top:15px;">
     
                <table class="table table-bordered">
                  <tr>
                    <th colspan="4" style="text-align:center">SISTEM IRIGASI UTAMA</th>
                    <th  colspan="4" style="text-align:center">SISTEM IRIGASI TERSIER</th>
                    <th  rowspan="2" style="vertical-align: middle; ">NILAI TOTAL</th>
                  </tr>
                  <tr>
                    <th>No. </th>
                    <th style="text-align:center">KOMPONEN</th>
                    <th>Indeks Kondisi </th>
                    <th>Bobot(50%)</th>

                    <th>No. </th>
                    <th style="text-align:center">KOMPONEN</th>
                    <th>Indeks Kondisi </th>
                    <th>Bobot(50%)</th>
                    <th></th>

                  </tr>
                  <?php
                  $tot_kondisi_u = $tot_kondisi_t = $tot_bobot_u = $tot_bobot_t = $sub_bobot_t = $sub_bobot_u = $total_all = $sub_total = 0;

                  for($i=0;$i<6;$i++){ 
                      $j = $i+1;
                      $u_compo = "u".$j;
                      $t_compo = "t".$j;
                      $sub_total_i = "sub_total".$i;
;

                      $sub_bobot_u = $data->$u_compo*$bobot_u;
                      $sub_bobot_t = $data->$t_compo*$bobot_t;
                      ${$u_compo} = $sub_bobot_u;
                      ${$t_compo} = $sub_bobot_t;


                      $tot_bobot_u += $sub_bobot_u;
                      $tot_bobot_t += $sub_bobot_t;

                      $sub_total = $sub_bobot_u+$sub_bobot_t;
                      $total_all += $sub_total;

                      ${$sub_total_i} = $sub_total;

                      $tot_kondisi_u += $data->$u_compo;
                      $tot_kondisi_t += $data->$t_compo;
                    ?>
                  <tr>
                    <td><?=($i+1)?>.</td>
                    <td><?=$u_components[$i]['name']?></td>
                    <td style="text-align:center"><?=$data->$u_compo?>%</td>
                    <td style="text-align:center"><?=$sub_bobot_u?>%</td>

                    <td><?=($i+1)?>. </td>
                    <td><?=$t_components[$i]['name']?></td>
                    <td style="text-align:center"><?=$data->$t_compo?>%</td>
                    <td style="text-align:center"><?=$sub_bobot_t?>%</td>
                    <td style="text-align:center"><?=$sub_total?>%</td>
                  </tr>
                  <?php } ?>
                  <tr>
                  <td>&nbsp;</td><td>&nbsp;</td><td style="text-align:center"><?=$tot_kondisi_u?>%</td><td style="text-align:center"><?=$tot_bobot_u?>%</td>
                  <td>&nbsp;</td><td>&nbsp;</td><td style="text-align:center"><?=$tot_kondisi_t?>%</td><td style="text-align:center"><?=$tot_bobot_t?>%</td>
                  <td style="text-align:center"><?=$total_all?>%</td>

                  </tr>
                </table>
            </div>


            </div>

             <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 300px;"></div>
              <div class="chart" id="donut-chart" style="height: 300px;"></div>
            </div>


    </section>
    <!-- Morris.js charts -->
<script src="<?=assets_url()?>bower_components/raphael/raphael.min.js"></script>
<script src="<?=assets_url()?>bower_components/morris.js/morris.min.js?v1"></script>

<script>
  $(function () {
    "use strict";

    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [
        <?php for($i=0;$i<6;$i++){ 
            $j = $i+1;
            $u_compo = "u".$j;
            $t_compo = "t".$j;
            $u_value = round($data->$u_compo*100/$u_components[$i]['index'],2);
            $t_value = round($data->$t_compo*100/$t_components[$i]['index'],2);

            $c_value = round(($data->$u_compo+$data->$t_compo)*100/($u_components[$i]['index']+$t_components[$i]['index']),2);
            ?>
          {y: 'U<?=$j?>/T<?=$j?>/U<?=$j?>+T<?=$j?>', a: <?=$u_value?>, b: <?=$t_value?>, c: <?=$c_value?>},

        <?php }?>
      ],
      barColors: ['#64B5F6', '#2196F3','#1565C0'],
      xkey: 'y',
      ykeys: ['a', 'b','c'],
      labels: ['U', 'T','U+T'],
      hideHover: 'auto'

    });

       //DONUT CHART
    var donut = new Morris.Donut({
      element: 'donut-chart',
      defaultLabelColor: '#1976D2',
      resize: true,
      colors: ["#BBDEFB", "#64B5F6", "#2196F3",'#1E88E5', '#1976D2','#1565C0','#D2D7D3'],
      data: [
        <?php for($i=0;$i<6;$i++){ 
          $sub_total_i = "sub_total".$i;
          //$nilai = (${$sub_total_i}/$total_all)*100;
          ?>
          {label: "Komponen <?=($i+1)?>", value: <?=round(${$sub_total_i},2)?> },
        <?php }?>
          {label: "", value: <?=round(100-$total_all,2)?> },

      ],
      hideHover: 'auto'
    });


  });


</script>
       