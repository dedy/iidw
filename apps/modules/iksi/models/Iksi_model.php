<?php

class Iksi_model extends CI_Model
{
    var $is_count = false;
    var $data = array();

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function search($limit = 0, $page = 0, $sidx = '', $sord = '', $params = array())
    {
        if (isset($params['id']) && $params['id']) {
            if (is_array($params['id']))
                $this->db->where_in("a.id", $params['id']);
            else
                $this->db->where("a.id", $params['id']);
        }

        if ($this->is_count) {
            $this->db->select("count(a.id) as total");
        } else {
            $this->db->select("a.*");
            $this->db->select("b.*");
            $this->db->select("c.*");
            $this->db->select("d.*");
            $this->db->select("u1+u2+u3+u4+u5+u6 as total_u",false);
            $this->db->select("t1+t2+t3+t4+t5+t6 as total_t",false);
       }

             $this->db->join('irigasi d', 'd.id_irigasi = a.id_irigasi', 'left outer');
            $this->db->join('tab_kabupaten b', 'b.id_kabupaten = d.id_kabupaten', 'left outer');
            $this->db->join('tab_propinsi c', 'c.id_propinsi = b.id_propinsi', 'left outer');
           
        if (!$this->is_count) {
            if ($limit && $page) {
                $offset = ($page - 1) * $limit;
                if (!$offset)
                    $offset = 0;
                $this->db->limit($limit, $offset);
            }

            if ($sidx)
                $this->db->order_by($sidx, $sord);
        }

        $query = $this->db->get('irigasi_iksi a');
        $result = $query->result();

        //echo $this->db->last_query();
        if (!$this->is_count)
            return $result;
        else
            return $result[0]->total;

    }


    public function count_search($params = array()) {
        $this->is_count = true;
        $res = $this->search(0, 0, '', '', $params);
        return $res;
    }

    public function insert($values)
    {
        $values['created_on'] = date("Y-m-d H:i:s");
        $values['created_by'] = _UserId();
        $values['updated_by'] = _UserId();
        $this->db->insert("regions", $values);
        return $this->db->insert_id();
    }

    public function update($data, $id)
    {
        $data["updated_on"] = date("Y-m-d H:i:s");
        $data["updated_by"] = _userid();
        $this->db->where('id', $id);
        $this->db->update('regions', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('regions');
    }

    public function name_exists($name, $id)
    {
        $this->db->where("name", $name);
        if ($id) {
            $this->db->where("id <>", $id);
        }
        $this->db->from('regions');

        $result = $this->db->get()->result();

        if (count($result)) {
            return true;
        } else {
            return false;
        }
    }

}