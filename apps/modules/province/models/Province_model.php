<?php

class Province_model extends CI_Model
{
    var $is_count = false;
    var $data = array();

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function search($limit = 0, $page = 0, $sidx = '', $sord = '', $params = array())
    {
        if (isset($params['id']) && $params['id']) {
            if (is_array($params['id']))
                $this->db->where_in("id_propinsi", $params['id']);
            else
                $this->db->where("id_propinsi", $params['id']);
        }
        
        if(isset($params['n_propinsi']) && $params['n_propinsi'] ){
            $this->db->like('LOWER(n_propinsi)',strtolower($params['n_propinsi']));
        }
     
        if ($this->is_count) {
            $this->db->select("count(a.id_propinsi) as total");
        }
    
        if (!$this->is_count) {
            if ($limit && $page) {
                $offset = ($page - 1) * $limit;
                if (!$offset)
                    $offset = 0;
                $this->db->limit($limit, $offset);
            }

            if ($sidx)
                $this->db->order_by($sidx, $sord);
        }

        $query = $this->db->get('tab_propinsi a');
        $result = $query->result();

        //echo $this->db->last_query();
        if (!$this->is_count)
            return $result;
        else
            return $result[0]->total;

    }


    public function count_search($params = array()) {
        $this->is_count = true;
        $res = $this->search(0, 0, '', '', $params);
        return $res;
    }

   

}