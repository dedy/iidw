<script src="<?php echo assets_url(); ?>/js/jquery.form.js" type="text/javascript"></script>
<link href='<?php echo assets_url(); ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo assets_url(); ?>js/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo assets_url(); ?>js/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo assets_url(); ?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<!-- jQuery UI -->
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.core.min.js"></script>
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.datepicker.min.js"></script>
<!--jqconfirm -->
<script src="<?php echo assets_url(); ?>js/jquery.confirm.min.js"></script>
<style type="text/css">
    .ui-jqgrid tr.jqgrow td {vertical-align:top !important}
</style>
<script type="text/javascript">
    jQuery().ready(function (){

            
         jQuery("#list1").jqGrid({
            url:'<?=site_url('log/email/loadDataGrid')?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','<?=lang('lfrom')?>','<?=lang('lto')?>','CC','<?=lang('lsubject')?>','<?=lang('lstatus')?>','<?=lang('ldate')?>'],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:20, align:"right",sortable:false},
                {name:'id',index:'log_id', hidden: true},
                {name:'from',index:'log_from',align:"left",stype:'text',width:250},
                {name:'to',index:'log_to',align:"left",stype:'text',width:200},
                {name:'cc',index:'log_cc',align:"left",stype:'text',width:200},
                {name:'subject',index:'log_subject',align:"left",stype:'text',width:250,
                    formatter: function (cellvalue, options, rowObject) {
                       return '<a href="<?=site_url('log/email/view')?>/' + rowObject[1] + '"><u>'+cellvalue+'</u></a>';
                    }
                },
                {name:'status_str',index:'status_str',align:"left",stype:'text',sortable:false,width:75},
                {name:'date',index:'created_on',align:"left",stype:'text'},
              ],
            rowNum:<?=$rowNum?>,
            <?if(isset($rowList) && $rowList){?>
                rowList:[<?=$rowList?>],
            <?}?>
            width: <?=config_item('jqgrid_width')?>,
            height: <?=config_item('jqgrid_height')?>,
            pager: '#pager1',
            viewrecords: true,
            caption:"<?=$title_page?>",
            sortname: 'created_on',
            sortorder: "desc" ,
            toppager: true, 
            shrinkToFit:false,
            loadComplete: function(data) {
                $("#total_records").html("("+data.records+")");
           }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
         
    });
    
    
    function gridReload(){
            jQuery("#list1").jqGrid('setGridParam',{
                url:"<?=site_url('log_email/loadDataGrid')?>    ",
                page:1
            }).trigger("reloadGrid");
    }


</script>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?=base_url()?>">Home</a>
        </li>
        <li><a href="#"><?=$title_page?></a></li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title>
                <h2><i class="glyphicon glyphicon-list-alt"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12" style="margin-top:30px">
                        <div id='show_message' style="display: none;"></div>
                        <table id="list1"></table> <!--Grid table-->
                        <div id="pager1"></div>  <!--pagination div-->
                    </div>
                </div>
            </div>
        </div>
    </div><!--/span-->

</div><!--/row-->
