
<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('/')?>">Home</a>
		</li>
		<li><a href="<?=site_url('log/email')?>"><?=$title_page?></a></li>
		<li><?=lang('lview')?></li>
	</ul>
</div>




<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title>
                <h2><i class="glyphicon glyphicon-list-alt"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
            </div>
            <div class="box-content">
                <div class="row">

                    <div class="col-md-12" style="margin-top:30px">
                        <table class="table table-striped">
                          <tbody>
                              <tr><td width='100px'><?=lang('ldate');?></td><td>:&nbsp;<?=$data->emails_created_on_fmt?></td></tr>
                              <tr><td><?=lang('lsubject');?></td><td>:&nbsp;<?=$data->log_subject?></td></tr>
                              <tr><td><?=lang('lfrom');?></td><td>:&nbsp;<?=$data->log_from?></td></tr>
                              <tr><td><?=lang('lto');?></td><td>:&nbsp;<?=$data->log_to?></td></tr>
                              <tr><td><?=lang('lcc');?></td><td>:&nbsp;<?=$data->log_cc?></td></tr>
                              <tr><td><?=lang('lbcc');?></td><td>:&nbsp;<?=$data->log_bcc?></td></tr>
                              <tr><td><?=lang('lstatus');?></td><td>:&nbsp;<?=$data->status_str?></td></tr>
                              <tr><td><?=lang('lerror_message');?></td><td>:&nbsp;<?=$data->message?></td></tr>
                              <tr><td><?=lang('lemail_content');?></td><td>:&nbsp;<?=$data->content?></td></tr>
                          </tbody>
                      </table>
                     <a href="<?=base_url()?>admin/log/email/">
                          <button class="btn btn-white" id='cancel-btn'><- <?=lang('lback')?></button>
                      </a>
                    
                    </div>


                </div>
            </div>
        </div>
    </div><!--/span-->

</div><!--/row-->