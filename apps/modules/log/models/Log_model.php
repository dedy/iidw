<?php
class Log_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
    function get_data_by_id($id){
        $data = $this->search(1,1,'id','asc',array("id"=>$id));
        
        if(isset($data[0])){
            return $data[0];
        }else{
            return false;
        }
    }
    
	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where("id",$searchval['id']);    
        }
       	if($this->is_count){
			$this->db->select("count(email_logs.id) as total");
			
		}else{
            $this->db->select("email_logs.*");
            $this->db->select("from as log_from");
            $this->db->select("to as log_to");
            $this->db->select("cc as log_cc");
            $this->db->select("bcc as log_bcc");
            $this->db->select("subject as log_subject");
            $this->db->select("content as log_content");
            $this->db->select("CASE status WHEN '1' THEN 'Sent' ELSE 'Not Sent' END as status_str",false);
            $this->db->select("DATE_FORMAT(created_on,'%d/%m/%Y %H:%i:%s') as emails_created_on_fmt",false);
       }
        
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('email_logs');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
}