<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
		
class Email extends MY_Controller{
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		//only superadmin can access
		if(!isAdmin()){
			redirect('auth/logout');
		}
        
        parent::__construct();
        $this->load->model('log/log_model');
        
	}
	
    function index(){
		$this->lists();
	}
	
    function email(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('lemail_log');
	
		parent::viewpage("vlog_list");
	}
    
   
    function view($id){
    	$data = $this->log_model->get_data_by_id($id);
        $this->viewparams['title_page']	= lang('lemail_log');
        $this->viewparams['data']	= $data;
		parent::viewpage("vlog_view");
	}
    
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'created_on'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'desc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array();
      	$query = $this->log_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->log_model->countSearch($searchv);
        
		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("id","log_from","log_to","log_cc","log_subject","status_str","emails_created_on_fmt"),
			"id"		=> "id"
 		);
            
        parent::loadDataJqGrid();
		
	}
    
	
	
}

/* End of file category.php */
/* Location: ./apps/modules/category/controllers/category.php */
