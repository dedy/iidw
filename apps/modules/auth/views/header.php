<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>Seefar Project Database</title>
     <meta http-equiv="refresh" content="120" />
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Dedy Adhiewirawan">
    <meta name="google-signin-client_id" content="222028723156-3sjjvvuhf3i1b29jbg4enbn1sgos6j12.apps.googleusercontent.com">

    <!-- The styles -->
    <!--<link id="bs-css" href="<?=assets_url()?>css/bootstrap-cerulean.min.css" rel="stylesheet">-->
    <link id="bs-css" href="<?=assets_url()?>css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?=assets_url()?>css/charisma-app.css" rel="stylesheet">
    <link href="<?=assets_url()?>css/custom-app.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="<?=assets_url()?>bower_components/jquery/jquery.min.js"></script>

    <!-- library for cookie management -->
    <script src="<?=assets_url()?>js/jquery.cookie.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    
<link rel="apple-touch-icon" sizes="180x180" href="<?=assets_url()?>img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?=assets_url()?>img/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?=assets_url()?>img/favicon/favicon-16x16.png" sizes="16x16">
<link rel="mask-icon" href="<?=assets_url()?>img/favicon/safari-pinned-tab.svg" color="#5bbad5">



<script>
    $(document).ready(function(){

        $(document).ajaxError(function() {
            alert('<?=lang('lerror_ajax')?>\r\n<?=lang('lerror_ajax2')?>');
            //window.location.reload(true);
        });

         $(document).ajaxStart(function() {
             $("#ajaxStatusDisplay_userMessage").show();

        });

        $( document ).ajaxStop(function() {
             $("#ajaxStatusDisplay_userMessage").hide();
        });


    });
    </script>   
<style type="text/css">
.ajaxStatusDisplay_userStyle {
    z-index: : 999;
    display: none;
    position:fixed;
    left:45%;top:0px;height:20;
    background-color:#F2F760;
    color:black;
    padding-left: 5px;
    padding-right: 5px;
    width:75px;
    font-weight: normal;
    font-family:Arial, Helvetica, sans-serif;
  
}

</style>
</head>
<body>
<div id="ajaxStatusDisplay_userMessage" class='ajaxStatusDisplay_userStyle'>
Loading...
</div>