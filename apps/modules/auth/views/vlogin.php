<?php include "header.php" ?>

<div class="ch-container">

    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Welcome to Seefar Project Database</h2>
        </div>
        <!--/span-->
    </div><!--/row-->

        <div class="row">
            <div class="well col-md-5 center login-box">
                <?php if($this->session->flashdata('message')):?>
                <div class="alert alert-danger">
                    <?=$this->session->flashdata('message')?>
                </div>
                <?php else:?>
                <div class="alert alert-info">
                    Please login with your Email and Password.
                </div>
                <?php endif?>
                  <?php echo form_open("auth/dologin",array('id'=>'login-form','class'=>'form-horizontal'));?>    <fieldset>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                            <?php echo form_input($identity);?>
              

                        </div>
                        <div class="clearfix"></div><br>

                        <div class="input-group input-group-lg">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                            <?php echo form_input($password);?>
                        </div>
                        <div class="clearfix"></div>

                       <div class="clearfix"></div>
                        
                        <p class="center col-md-8">
                             <div id='show_message' style="margin:5px 0 0 0"></div>
                        </p>

                        <p class="center col-md-5">
                            <button id='submit-btn' type="submit" class="btn btn-primary">Login</button>
                        </p>
                        
                        <div class="input-prepend">
                            <label class="remember" for="remember"><a href='<?=site_url('forgot_password')?>'>Forgot Your Password ?</a></label>
                        </div>

                        <div class="input-prepend">
                            <label>OR </label>
                        </div>

                        <div class="center col-md-6">
                           <div class="center" id="my-signin2"></div>
                        </div>
                        
                            <?php echo form_close();?>

                         


                       
                    </fieldset>
            </div>
            <!--/span-->
        </div><!--/row-->

          <footer class="row">
           <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://seefar.org" target="_blank">Project Database</a> 2017</p>
        </footer>

</div><!--/.fluid-container-->
<script>

    function onSuccess(googleUser) {
        var id_token = googleUser.getAuthResponse().id_token;
       // var domain = str.split("@");

       // if (domain[1] == '<?=config_item('email_login')?>') {
            window.location="<?=base_url()?>auth/doLoginGoogle?username="+googleUser.getBasicProfile().getEmail()+"&csrf_token="+id_token;
      //  } else {
      //      window.location="https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?=base_url()?>auth/logout";
      //  }
    }

    function onFailure(errot) {
        console.log(errot);
    }

    function renderButton() {
        gapi.signin2.render('my-signin2', {
            'scope': 'profile email',
            'width': 240,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        });
    }

    $(document).ready(function(){
        $('#identity').focus();

        $('#submit-btn').click(function(e){
            //$('form#login-form').submit();
            //return;
            e.preventDefault(); 
       
            $.post('<?=site_url('auth/dologin');?>', 
                $("#login-form").serialize(),
                function(returData) {
                    $('#show_message').slideUp('normal',function(){
                      //  alert(returData.message);
                        if(returData.error){

                            var rv = ' <div class="alert alert-danger">'+returData.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal'); 
                        }else{
                            var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal',function(){
                                 setTimeout(function() {
                                    $('#show_message').slideUp('normal',function(){
                                        if(returData.redirect){
                                            window.location.replace(returData.redirect);
                                        }
                                    }); 
                                  }, '<?=config_item('message_delay')?>');
                            }); 
                        }   
                    });
                    
                },'json');
        });
        
    });
</script>   
<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>


</body>
</html>
