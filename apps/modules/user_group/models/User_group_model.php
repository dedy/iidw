<?php
class User_group_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
	function get_data($id){
		
		$this->db->where('user_id',$id);
        $this->db->from('users_groups');

		$query = $this->db->get();
		
 		return $query->result();
	}
	

    

}