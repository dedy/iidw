<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Irigasi extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
        $this->load->model("irigasi_model");
		$this->load->model("province/province_model");
	}

	public function index() {
        parent::set_menu("data","daerah_irigasi");
        //get list of province
        
        $provinces = $this->province_model->search(0,1,'n_propinsi','asc');

        $this->viewparams['provinces'] = $provinces;
        $this->viewparams['title_page']	= lang('lirigasi');
        parent::viewpage("vlist");
	}

    public function loadDataGrid() {
        $page = isset($_POST['page'])?$_POST['page']:1;
        $sidx = isset($_POST['sidx'])?$_POST['sidx']:'n_kabupaten'; // get index row - i.e
        $sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
        $limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid


        $params = array(
            'n_kabupaten' => $this->input->get('n_kabupaten'),
            'n_irigasi' => $this->input->get('n_irigasi'),
            'id_propinsi' => $this->input->get('id_propinsi')
        );

        $query = $this->irigasi_model->search($limit,$page,$sidx,$sord,$params);
        $this->firephp->log($this->db->last_query());
        $count = $this->irigasi_model->count_search($params);
        $this->firephp->log($this->db->last_query());
        $this->firephp->log("count : ".$count);
        for($i=0;$i<count($query);$i++){
            $query[$i]->delete =  $query[$i]->edit = "";
            $query[$i]->luas = number_format($query[$i]->luas);

        }
     
        $this->DataJqGrid = array(
            "page"		=> $page,
            "sidx"		=> $sidx,
            "sord"		=> $sord,
            "query" 	=> $query,
            "limit"		=> $limit,
            "count"		=> $count,
            "column"	=> array("id_irigasi","k_di","n_di","n_propinsi","n_kabupaten","luas","edit","delete"),
            "id"		=> "id_irigasi"
        );
        parent::loadDataJqGrid();

    }

    public function get_active(){
        $id = $this->input->get('id');
        $record = $this->region_model->get_row(array('id' => $id));
        if($record){
            $data['active'] = $record->active;
            echo json_encode($data);
        }
    }

    public function set_active($id){
        $record = $this->region_model->get_row(array('id' =>$id));
        if($record){
            $active = $record->active;
            if($active == '1')
                $data['active'] = '0';
            else
                $data['active'] = '1';
            $this->region_model->update($data,$id);
            $message = ($data['active'] == '1')?lang('ldata_set_active'):lang('ldata_set_inactive');
        } else {
            $message = lang('lno_data_on_database');
        }

        $result = array("message" => $message);
        echo json_encode($result);
    }

    public function add(){
        $this->viewparams['title_page']	= lang('ladd_region');
        $this->form(0);
    }

    public function edit($id){
        $data = $this->region_model->get_row(array('id' =>$id));
        if(!$data){
            redirect('region');
        }

        $this->viewparams['title_page']	= lang('lmodify_region');
        $this->viewparams['data'] = $data;
        $this->form($id);
    }

    public function form($id=0){
        parent::viewpage("vform");
    }

    //update_banner
    public function do_update(){
        if(!$this->input->is_ajax_request())
            redirect('auth/logout');

        /* check mandatory fields */
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $active	= $this->input->post('active');
        $message = array();
        $redirect = "";
        $is_error = false;

        //-- run form validation
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'lang:lname', 'required');
        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == FALSE){
            $is_error = true;
            $message[] = validation_errors();

        } else {
            if($name) {
                //check unique name
                $name_exists = $this->region_model->name_exists($name,$id);
                $this->firephp->log($this->db->last_query());
                if($name_exists){
                    $is_error = true;
                    $message[] = lang('lname_exists');
                }
            }
        }

        if(!$is_error){
            /* add/update if no error */
            if(!$is_error){
                $data = array(
                    "name" => $name,
                    "active" => ($active) ? $active : "0",
                );

                //add
                if(!$id){
                    $id = $this->region_model->insert($data);

                    if($id)
                        $message[] = lang('ldata_success_inserted');
                    else
                        $message[] = lang('ldata_failed_inserted');

                }else{

                    $this->region_model->update($data,$id);
                    $message[] = lang('ldata_success_updated');
                }

                $redirect = base_url()."region";
            }
        }

        $result = array(
            "message"	=> implode("<br>",$message),
            "error"	=> $is_error,
            "redirect"	=> $redirect
        );

        echo json_encode($result);
    }

    public function delete($id) {
        $data =  $this->region_model->get_row(array('id' => $id));
        if($data){
            $this->region_model->delete($id);
			$error = $this->db->error();
	        if($error['code'] == 1451) {
		        $success = false;
		        $message = lang('lused_in_another_data');
	        }elseif($this->db->affected_rows()){
		        $success = true;
		        $message = lang('ldata_is_deleted');
	        } else {
		        $success = false;
		        $message = lang('ldata_is_failed_deleted');
	        }
        }else {
            $success = false;
	        $message = lang('lno_data_on_database');
        }

        $result = array("success"=>$success,"message" => $message);
        echo json_encode($result);
    }
}
