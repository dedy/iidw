<script src="<?php echo assets_url(); ?>/js/jquery.form.js" type="text/javascript"></script>
<link href='<?php echo assets_url(); ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo assets_url(); ?>js/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo assets_url(); ?>js/jqgrid/css/ui.jqgrid.css?v1);</style>
<!-- jQuery UI -->
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.core.min.js"></script>
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.datepicker.min.js"></script>
<!--jqconfirm -->
<script src="<?php echo assets_url(); ?>js/jquery.confirm.min.js"></script>
<style type="text/css">
    .chosen-container-multi .chosen-choices li.search-field input[type=text] {
        height: 25px;
    }
</style>

<script>
    $(document).ready(function(){
        $('.chosen').chosen({ allow_single_deselect: true });

        $('#cancel-btn').click(function(e){
            e.preventDefault();
            window.location.replace("<?=site_url('user');?>");
        });

        $('#submit-btn').click(function(e){
            //$('form#login-form').submit();
            //return;
            e.preventDefault();
            dopost($(this));
        });

        $('.datepicker').datepicker(
            {
                changeMonth: true
                ,changeYear: true
                ,beforeShow: function (textbox, instance) {
                var txtBoxOffset = $(this).offset();
                var top = txtBoxOffset.top;
                var left = txtBoxOffset.left;
                var textBoxHeight = $(this).outerHeight();
                setTimeout(function () {
                    instance.dpDiv.css({
                        top: top-$("#ui-datepicker-div").outerHeight(),
                        left: left
                    });
                }, 0);
            }
                ,dateFormat: "yy-mm-dd"
            }
        );

    });

    function dopost(obj){
        obj.attr('disabled','disabled');
        $('#loadingmessage').show();

        $.post('<?=site_url('user/do_update');?>',
            $("#input-form").serialize(),
            function(returData) {

                $('#loadingmessage').hide();
                obj.removeAttr('disabled');

                $('#show_message').slideUp('normal',function(){

                    if(returData.error){
                        var rv = '<div class="alert alert-danger">'+returData.message+'</div>';
                        $('#show_message').html(rv);
                        $('#show_message').slideDown('normal');

                    }else{
                        var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                        $('#show_message').html(rv);
                        $('#show_message').slideDown('normal',function(){
                            setTimeout(function() {
                                $('#show_message').slideUp('normal',function(){
                                    if(returData.redirect){
                                        window.location.replace(returData.redirect);
                                    }
                                });
                            }, <?=config_item('message_delay')?>);
                        });
                    }
                });

            },'json');
    }
</script>

<div>
    <ul class="breadcrumb">
        <li> <a href="<?=site_url('/')?>"><?=lang('lhome')?></a></li>
        <li> <a href="<?=site_url('/user')?>"><?=lang('luser')?></a></li>
        <li><a href="#"><?=$title_page?></a></li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <?=$title_page?></h2>
            </div>

            <div class="box-content">
                <?php $hidden = array("id" => (isset($data->user_id))? $data->user_id:0); ?>
                <?php echo form_open("user/do_update",array('role'=>"form",'id'=>'input-form' ,'class'=>'form-horizontal'),$hidden)?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><?=lang('lgroup')?><span class="required">* </span></label>
                            <div class="col-md-8">
                                <select name='group_id' id='group_id' class="form-control chosen"  data-placeholder="<?=lang('lplease_select_group')?>" >
                                    <option value=''></option>
                                    <?php
                                    foreach($groups as $value){
                                        if(!isAdmin()){

                                            if($value->id == 1){
                                                continue;
                                            }
                                        }
                                        ?>
                                        <option value='<?=$value->id?>' <?php if(isset($data->group_id) && ($data->group_id == $value->id)) { echo "selected"; } ?>><?=ucfirst($value->description)?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><?=lang('lfull_name')?> <span class="required">* </span></label>
                            <div class="col-md-8">
                                <input type="text" id="full_name" name="full_name" data-required="1"  class="form-control" maxlength="100" value="<?php if(isset($data->full_name) && $data->full_name) echo $data->full_name ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><?=lang('lemail')?> <span class="required">* </span></label>
                            <div class="col-md-8">
                                <input type="text" id="username" name="username" data-required="1"  class="form-control" maxlength="100" value="<?php if(isset($data->username) && $data->username) echo $data->username ?>"/>
                            </div>
                        </div>

                        

                    </div> <!-- /end col-md-6 -->
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="control-label col-md-4"><?=isset($data)? lang('lchange_password'):lang('lpassword')?></label>
                            <div class="col-md-8">
                                <input type="password" id="password" name="password" data-required="1"  class="form-control" maxlength="50"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><?=lang('lretype_password')?><span class="required"></label>
                            <div class="col-md-8">
                                <input type="password" id="retype_password" name="retype_password" data-required="1"  class="form-control" maxlength="50"/>
                            </div>
                        </div>


                         <div class="form-group">
                                <label class="control-label col-md-4" for="active"><?=lang('lactive');?></label>
                                <div class="col-md-8">
                                     <?php
                                     if(isset($data) && ($data->user_id == 1)) { ?>
                                        <input type='hidden' name='active' value='<?=$data->active?>'>
                                         <label class="radio-inline">
                                            <?php
                                            if($data->active == "1") {
                                                echo lang('lyes');
                                            }else{
                                                echo lang('lno');
                                            }
                                            ?>
                                         </label>
                                     <?php } else { ?> 
                                         <label class="radio-inline">
                                            <input type="radio" name="active" id="active" value="1" <?php if( (isset($data->active) && ($data->active == "1")) || !isset($data->active)){ echo "checked";}?>> <?=lang('lyes')?>
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="active"  id="active" value="0" <?php if( (isset($data->active) && ($data->active == "0"))){ echo "checked";}?>><?=lang('lno')?>
                                        </label>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <label class="control-label col-md-4" for="locked"><?=lang('llocked');?></label>
                                <div class="col-md-8">
                                    <?php
                                     if(isset($data) && ($data->user_id == 1)) { ?>
                                        <input type='hidden' name='is_locked' value='<?=$data->is_locked?>'>
                                         <label class="radio-inline">
                                            <?php
                                            if($data->is_locked == "1") {
                                                echo lang('lyes');
                                            }else{
                                                echo lang('lno');
                                            }
                                            ?>
                                         </label>
                                     <?php } else { ?> 
                                         <label class="radio-inline">
                                            <input type="radio" name="is_locked" id="is_locked" value="1"  <?php if( (isset($data->is_locked) && $data->is_locked)){ echo "checked";}?>> <?=lang('lyes')?>
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="locked" id="locked" value="0"  <?php if( (isset($data->is_locked) && ($data->is_locked<> "1") ) || !isset($data->is_locked)){ echo "checked";}?>><?=lang('lno')?>
                                        </label>
                                    <?php } ?>
                                </div>
                            </div>
                            -->

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div id='show_message' style="display: none;"></div>
                    </div>
                    <div class="col-md-12 text-center"> 
                          <button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsubmit')?></button>
                          <button type="button" class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
                    </div>
                </div>

                <?=form_close();?>

            </div><!-- end box-content -->

        </div><!-- /end box-inner -->
    </div>
</div>

<!-- select or dropdown enhancer -->
<script src="<?php echo assets_url(); ?>bower_components/chosen/chosen.jquery.min.js"></script>