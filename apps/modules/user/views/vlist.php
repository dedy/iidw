<script src="<?php echo assets_url(); ?>/js/jquery.form.js" type="text/javascript"></script>
<link href='<?php echo assets_url(); ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo assets_url(); ?>js/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo assets_url(); ?>js/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo assets_url(); ?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<!-- jQuery UI -->
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.core.min.js"></script>
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.datepicker.min.js"></script>
<!--jqconfirm -->
<script src="<?php echo assets_url(); ?>js/jquery.confirm.min.js"></script>
<style type="text/css">
    .ui-jqgrid tr.jqgrow td {vertical-align:top !important}
</style>

<script type="text/javascript">
    jQuery().ready(function (){

        $("#search-btn").click(function(e){
            e.preventDefault();
            $("#submit_spinner").show();
            gridReload();
        });

        jQuery("#list1").jqGrid({
            url:'<?=base_url()?>user/loadDataGrid',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','locked','active','<?=lang('lgroup')?>','<?=lang('lfull_name')?>','<?=lang('lemail')?>','<?=lang('lactive')?>','<?=lang('llocked')?>','<?=lang('ledit')?>','<?=lang('ldel')?>'],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:10, align:"right",sortable:false},
                {name:'id',index:'id', hidden: true},
                {name:'locked',index:'locked', hidden: true},
                {name:'active1',index:'active1', hidden: true},
                {name:'group_name',index:'user_group_description',width:45,align:"left",stype:'text'},
                {name:'full_name',index:'full_name',width:65,align:"left",stype:'text'},
                {name:'username',index:'email',width:45,align:"left",stype:'text'},
                {name:'active', index: 'active', width: 15,align:"center",sortable:false,
                    formatter: 'dynamicLink',
                    formatoptions: {
                        url: function (cellValue, rowId, rowData) {
                            return '#';
                        },
                        cellValue: function (cellValue, rowId, rowData) {

                            if(cellValue == "1")
                                return "<img src='<?=base_url()?>assets/img/ico-yes.png' id='active-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";
                            else
                                return "<img src='<?=base_url()?>assets/img/ico-standby.png' id='active-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";

                            //return cellValue;
                        },
                        onClick: function (rowId, iRow, iCol, cellValue, e) {
                            //admin can not be set inactive
                            if(rowId == 1){
                                return false;
                            }

                            var active = $('#list1').jqGrid('getCell',rowId,'active1');
                            var fullName = $('#list1').jqGrid('getCell',rowId,'full_name');
                            
                            var msg = '';
                            var result='';

                            if(active == '1') {
                                msg     = '\''+fullName+'\' <?=lang('lwill_inactive')?>';
                                result  = '\''+fullName+'\' <?=lang('lhas_inactive')?>';
                            } else {
                                msg     = '\''+fullName+'\' <?=lang('lwill_active')?>';
                                result  = '\''+fullName+'\' <?=lang('lhas_active')?>';
                            }

                            $.confirm({
                                title:"<?=lang('lchange_status_confirmation')?>",
                                text: msg,
                                confirmButton: "<?=lang('lok')?>",
                                cancelButton: "<?=lang('lcancel')?>",
                                confirm: function(button) {
                                    $.post("<?=base_url()?>user/set_active/"+rowId,
                                        function(data) {
                                            var rv = '<div class="alert alert-success">' + result + '</div>';
                                            $('#show_message').html(rv);
                                            $('#show_message').slideDown('normal',function(){
                                                setTimeout(function() {
                                                    $('#show_message').slideUp('normal',function(){
                                                        gridReload();
                                                    });
                                                }, <?=config_item('message_delay')?>);
                                            });


                                        },"json"
                                    );
                                },
                                cancel: function(button) {

                                }
                            });

                            $.getJSON("<?=base_url()?>user/get_active",{id:rowId},function(response) {
                                var active = response.active;
                                
                            });
                        }},
                    cellattr: function (rowId, cellValue, rawObject) {
                        //column 2 , status
                        if(rawObject[2] == "1")
                            var attribute = ' title="<?=lang('lset_inactive')?>"' ;
                        else
                            var attribute = ' title="<?=lang('lset_active')?>"' ;

                        return attribute ;
                    }
                },
                {name:'is_locked', index: 'is_locked', width: 15,align:"center",sortable:false,
                    formatter: 'dynamicLink',
                    formatoptions: {
                        url: function (cellValue, rowId, rowData) {
                            return '#';
                        },
                        cellValue: function (cellValue, rowId, rowData) {
                            if(cellValue == "1")
                                return "<img src='<?=base_url()?>assets/img/locked.png' id='locked-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";
                            else
                                return "";
                              //  return "<img src='<?=base_url()?>assets/img/unlocked.png' id='unlock-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";

                            //return cellValue;
                        },
                        onClick: function (rowId, iRow, iCol, cellValue, e) {
                           var is_locked = $('#list1').jqGrid('getCell',rowId,'locked');
                           var fullName = $('#list1').jqGrid('getCell',rowId,'full_name');
                           var msg = '';
                           var result='';

                            if(is_locked == 1) {
                                msg     = '\''+fullName+'\' <?=lang('lwill_unlock')?>';
                                result  = '\''+fullName+'\' <?=lang('lhas_unlocked')?>';
                            } else {
                                msg     = '\''+fullName+'\' <?=lang('lwill_lock')?>';
                                result  = '\''+fullName+'\' <?=lang('lhas_locked')?>';
                            }

                            $.confirm({
                                title:"<?=lang('lchange_status_confirmation')?>",
                                text: msg,
                                confirmButton: "<?=lang('lok')?>",
                                cancelButton: "<?=lang('lcancel')?>",
                                confirm: function(button) {
                                    $.post("<?=base_url()?>user/set_locked/"+rowId,
                                        function(data) {
                                            var rv = '<div class="alert alert-success">' + result + '</div>';
                                            $('#show_message').html(rv);
                                            $('#show_message').slideDown('normal',function(){
                                                setTimeout(function() {
                                                    $('#show_message').slideUp('normal',function(){
                                                        gridReload();
                                                    });
                                                }, <?=config_item('message_delay')?>);
                                            });


                                        },"json"
                                    );
                                },
                                cancel: function(button) {

                                }
                            });

                            $.getJSON("<?=base_url()?>user/get_locked",{id:rowId},function(response) {
                                var is_locked = response.is_locked;
                                
                            });
                        }},
                    cellattr: function (rowId, cellValue, rawObject) {
                        //column 2 , status
                        if(rawObject[2] == "1")
                            var attribute = ' title="<?=lang('lset_unlocked')?>"' ;
                        else
                            var attribute = ' title="<?=lang('lset_locked')?>"' ;

                        return attribute ;
                    }
                },
                {name:'edit',index:'edit', width:12, align:"left",sortable:false,align:"center",
                    formatter:'dynamicLink',
                    formatoptions:{
                        url: function (cellValue, rowId, rowData) {
                            return '<?=base_url()?>user/edit/' + rowId;
                        },
                        cellValue: function (cellValue, rowId, rowData) {
                            return "<img src='<?=base_url()?>assets/img/ico-edit.png' id='edit-"+rowId+"' class='edit' border='0' width='16px' height='16px'>";
                        }
                    },
                    cellattr: function (rowId, cellValue, rawObject) {
                        var attribute = ' title="<?=lang('ledit')?>"'
                        return attribute ;
                    }
                },
                {name: 'delete', index: 'delete', width: 12,align:"center",
                    formatter: 'dynamicLink',
                    formatoptions: {
                        url: function (cellValue, rowId, rowData) {
                            return '<?=base_url()?>user/delete/' + rowId;
                        },
                        cellValue: function (cellValue, rowId, rowData) {
                            if(rowId == 1){
                                return "";
                            }

                            return "<img src='<?=base_url()?>assets/img/ico-delete.png' id='delete-"+rowId+"' class='delete' border='0' width='16px' height='16px'>";
                        },
                        onClick: function (rowId, iRow, iCol, cellValue, e) {
                            $.confirm({
                                title:"<?=lang('ldelete')?>",
                                text: "<?=lang('ldelete_confirmation')?>",
                                confirmButton: "<?=lang('lok')?>",
                                cancelButton: "<?=lang('lcancel')?>",
                                confirm: function(button) {
                                    $.post(
                                        "<?=base_url()?>user/delete/" + rowId,
                                        function(data) {
                                            if(data.success)
                                                var rv = '<div class="alert alert-success">'+data.message+'</div>';
                                            else
                                                var rv = '<div class="alert alert-danger">'+data.message+'</div>';

                                            $('#show_message').html(rv);
                                            $('#show_message').slideDown('normal',function(){
                                                setTimeout(function() {
                                                    $('#show_message').slideUp('normal',function(){
                                                        if(data.success){
                                                            gridReload();
                                                        }
                                                    });
                                                }, <?=config_item('message_delay')?>);
                                            });
                                            //reload grid
                                            //gridReload();
                                        },"json"
                                    );

                                },
                                cancel: function(button) {
                                    // alert("You cancelled.");
                                }
                            });

                        }
                    },
                    cellattr: function (rowId, cellValue, rawObject) {
                        var attribute = ' title="<?=lang('ldelete')?>"'
                        return attribute ;
                    }}
            ],
            rowNum:<?=$rowNum?>,
            <?if(isset($rowList) && $rowList){?>
            rowList:[<?=$rowList?>],
            <?}?>
              width: null,
            autowidth: true,height: <?=config_item('jqgrid_height')?>,
           pager: '#pager1',
            viewrecords: true,
            caption:"<?=lang('luser_list')?>",
            sortname: 'user_group_description,full_name,username',
            toppager: true,
            loadComplete: function(data) {
                $("#total_records").html("("+data.records+")");
            }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});

    });


    function gridReload() {
        var username =(jQuery("#username").val())?jQuery("#username").val():'';
        var full_name =(jQuery("#full_name").val())?jQuery("#full_name").val():'';
        var active = $('input[name="active"]:checked', '#input-form').val();
        var locked = $('input[name="locked"]:checked', '#input-form').val();
        var group_id = (jQuery("#group_id option:selected").val())?jQuery("#group_id option:selected").val():'0';

        jQuery("#list1").jqGrid('setGridParam',{
            url:"<?=site_url('user/loadDataGrid')?>/?username="+username+"&full_name="+full_name+"&active="+active+"&locked="+locked+"&group_id="+group_id,
            page:1
        }).trigger("reloadGrid");
        $("#submit_spinner").hide();
    }


</script>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?=site_url()?>">Home</a>
        </li>
        <li><a href="#"><?=$title_page?></a></li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
            </div>

              <div class="box-content">
                 <?php echo form_open("project/loadDataGrid",array('role'=>"form",'id'=>'input-form' ,'class'=>'form-horizontal'))?>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4"><?=lang('lgroup')?></label>
                                <div class="col-md-8">
                                    <select name='group_id' id='group_id' class="form-control"  data-placeholder="<?=lang('lplease_select_group')?>" >
                                        <option value=''></option>
                                        <?php
                                        foreach($groups as $value){
                                            ?>
                                            <option value='<?=$value->id?>' <?php if(isset($data->group_id) && ($data->group_id == $value->id)) { echo "selected"; } ?>><?=ucfirst($value->description)?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4" for="full_name"><?=lang('lfull_name');?></label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name='full_name' id='full_name' value="" maxlength='100' />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4" for="username"><?=lang('lemail');?></label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name='username' id='username' value="" maxlength='100' />
                                </div>
                            </div>
                            
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4" for="active"><?=lang('lactive');?></label>
                                <div class="col-md-8">
                                    <label class="radio-inline">
                                        <input type="radio" name="active" id="active" checked="checked" value="-1"> <?=lang('lall')?>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="active" id="active" value="1"> <?=lang('lactive')?>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="active"  id="active" value="0"><?=lang('linactive')?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" for="locked"><?=lang('llocked');?></label>
                                <div class="col-md-8">
                                    <label class="radio-inline">
                                        <input type="radio" name="locked" id="locked" checked="checked" value="-1"> <?=lang('lall')?>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="locked" id="locked" value="1"> <?=lang('lyes')?>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="locked" id="locked" value="0"><?=lang('lno')?>
                                    </label>
                                </div>
                            </div>
                        </div><!-- /end col-md-6 -->
                    </div><!-- end row -->

                    <div class="row">
                        <div class="col-sm-offset-5">
                            <button type="submit" class="btn btn-primary" id='search-btn'><?=lang('lsearch')?></button>                 
                            <a class="btn btn-primary" href="<?=base_url()?>user/add"><?=lang('ladd_new')?></a>
                        </div>
                    </div><!-- end row -->

                <?=form_close();?> 
                  
                    <div class="col-md-12">
                        <div class="col-md-12" style="margin-top:15px;">
                            <div id='show_message' style="display: none;"></div>
                            <table id="list1"></table> <!--Grid table-->
                            <div id="pager1"></div>  <!--pagination div-->
                        </div>
                    </div>
            </div><!-- end box-content -->

        </div><!-- /end box-inner -->
    </div><!-- end box -->
</div>