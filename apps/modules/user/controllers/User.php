<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {
	
	public function __construct() {
		
		parent::__construct();
		$this->load->model("user_model");
        $this->load->model('group/group_model');
        $this->load->model('user_group/user_group_model');
	   	$this->lang->load('auth');
     
	}

	public function index(){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $this->viewparams['groups'] = $this->group_model->search(0,0,'name','asc');
        $this->viewparams['title_page']	= lang('luser');
        parent::viewpage("vlist");
	}

    public function loadDataGrid() {
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $page = isset($_POST['page'])?$_POST['page']:1;
        $sidx = isset($_POST['sidx'])?$_POST['sidx']:'user_group_description,full_name,username'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
        $limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid


        $params = array(
            'username' => $this->input->get('username'),
            'full_name' => $this->input->get('full_name'),
            'active' => $this->input->get('active'),
            'group_id' => $this->input->get('group_id'),
            'locked' => $this->input->get('locked'),
        );

        $query = $this->user_model->search($limit,$page,$sidx,$sord,$params);
        $this->firephp->log($this->db->last_query());
        $count = $this->user_model->count_search($params);

        $this->firephp->log($this->db->last_query());
        $this->firephp->log("count : ".$count);
        for($i=0;$i<count($query);$i++){
            $query[$i]->delete =  $query[$i]->edit = "";
        }

        $this->DataJqGrid = array(
            "page"		=> $page,
            "sidx"		=> $sidx,
            "sord"		=> $sord,
            "query" 	=> $query,
            "limit"		=> $limit,
            "count"		=> $count,
            "column"	=> array("id","is_locked","active","user_group_description","full_name","username","active","is_locked","edit","delete"),
            "id"		=> "id"
        );
        parent::loadDataJqGrid();

    }

    public function get_active(){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $id = $this->input->get('id');
        $user = $this->user_model->get_user_data($id);
        if($user){
            $data['active'] = $user->active;
            echo json_encode($data);
        }
    }

    public function set_active($id){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $user = $this->user_model->get_user_data($id);
        if($user){
            $active = $user->active;
            if($active == 1)
                $data['active'] = '0';
            else
                $data['active'] = '1';
            $this->user_model->setActive($id,$data);
            $message = ($data['active'] == '1')?lang('ldata_set_active'):lang('ldata_set_inactive');
        } else {
            $message = lang('lno_data_on_database');
        }

        $result = array("message" => $message);
        echo json_encode($result);
    }

    public function get_locked(){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $id = $this->input->get('id');
        $user = $this->user_model->get_user_data($id);
        if($user){
            $data['is_locked'] = $user->is_locked;
            echo json_encode($data);
        }
    }

    public function set_locked($id){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $user = $this->user_model->get_user_data($id);
        if($user){
            $is_locked = $user->is_locked;
            if($is_locked  == 1){
                $data['is_locked'] = '0';

                $identity = $user->username;

                //clear login attempts
                $this->ion_auth->clear_login_attempts($identity);

            }
            else
                $data['is_locked'] = '1';
            $this->user_model->set_locked($id,$data);
            $message = ($data['is_locked'] == 1) ? lang('ldata_set_locked') : lang('ldata_set_unlocked');
        } else {
            $message = lang('lno_data_on_database');
        }

        $result = array("message" => $message);
        echo json_encode($result);
    }

    public function add(){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $this->viewparams['title_page']	= lang('ladd_user');
        $this->form(0);
    }

    public function edit($id){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $data = $this->user_model->get_user_data($id);
        if(!$data){
            redirect('user');
        }

        $this->viewparams['title_page']	= lang('lmodify_user');
        $this->viewparams['data'] = $data;
        $this->viewparams['data_group']	= $this->user_group_model->get_data($id);
        $this->form($id);
    }

    public function form($id=0){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $this->viewparams['groups'] = $this->group_model->search(0,0,'name','asc');
        parent::viewpage("vform");
    }

    //update_banner
    public function do_update(){
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        if(!$this->input->is_ajax_request())
            redirect('auth/logout');


        /* check mandatory fields */
        $id		        = $this->input->post('id');
        $group_id		= $this->input->post('group_id');
        $full_name        = $this->input->post('full_name');
        $username        = $this->input->post('username');
        $active	          = $this->input->post('active');
        //$is_locked          = $this->input->post('is_locked');
        
        //mandatory in add mode
        $password		= $this->input->post('password');
        $retype_password	= $this->input->post('retype_password');
        $message = array();
        $redirect = "";
        $is_error = false;

        //-- run form validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('group_id', 'lang:lgroup', 'required');
        $this->form_validation->set_rules('full_name', 'lang:lfull_name', 'required');
        $this->form_validation->set_rules('username', 'lang:lemail', 'required|valid_email');


        //add mode
       // if(!$id){
       //     $this->form_validation->set_rules('password', 'lang:lpassword', 'required|matches[retype_password]');
       //     $this->form_validation->set_rules('retype_password', 'lang:lretype_password', 'required');
      //  }else{
            if($password){
                $this->form_validation->set_rules('password', 'lang:lpassword', 'required|matches[retype_password]');
                $this->form_validation->set_rules('retype_password', 'lang:lretype_password', 'required');
            }
     //   }

        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == FALSE){
            $is_error = true;

            if($username) {
                //check unique email dan login name
                $data_exists = $this->user_model->username_exists($username,$id);
                $this->firephp->log($this->db->last_query());
                if($data_exists){
                    $message[] = lang('lemail-exists');
                }
            }

            $message[] = validation_errors();

        }

        if(!$is_error){

            /* add/update if no error */
            if(!$is_error){
                $data = array(
                    "full_name"         => $full_name,
                    "username"          => $username,
                    "email"          => $username,
                    "active"            => ($active) ? $active : "0",
                   // "is_locked"         => ($is_locked) ? $is_locked : "0",
                    "updated_by"        => _userid(),
                    "updated_on"        => time(),
                );

                //add
                if(!$id){
                    $data["created_by"]  = _userid();
                    $id = $this->ion_auth->register($username, $password, $username, $data, array($group_id));

                    if($id)
                        $message[] = lang('ldata_success_inserted');
                    else
                        $message[] = lang('ldata_failed_inserted');

                }else{
                    //if edit mode, and password and filled in , will reset password
                    if($password){
                        $data['password'] = $password;
                    }
                    $this->ion_auth->update($id, $data);

                    // First we removed this user from all groups
                    $this->ion_auth->remove_from_group(NULL, $id);

                    // then add him to selected group
                    $this->ion_auth->add_to_group($group_id , $id);

                    //insert into fields table relation
                    //$this->field_model->update($id,$fields);

                    $message[] = lang('ldata_success_updated');
                }

                $redirect = base_url()."user";
            }
        }

        $result = array(
            "message"	=> implode("<br>",$message),
            "error"	=> $is_error,
            "redirect"	=> $redirect
        );

        echo json_encode($result);
    }

    public function delete($id) {
        //only admin can access
        if(!isAdmin()){
            redirect('auth/logout','refresh');
        }
        
        $data =  $this->user_model->get_user_data($id);
        if($data){
            $this->user_model->delete(array('id' => $id));
			$error = $this->db->error();
            if($error['code'] == 1451) {
	            $success = false;
	            $message = lang('lused_in_another_data');
            }else if($this->db->affected_rows()){
                $success = true;
                $message = lang('ldata_is_deleted');
            } else {
                $success = false;
                $message = lang('ldata_is_failed_deleted');
            }
        }else {
            $success = false;
            $message = lang('lno_data_on_database');
        }

        $result = array("success"=>$success,"message" => $message,'mysql_code' =>$error['code'],'mysql_message' => $error['message']);
        echo json_encode($result);
    }



    function dochange_password(){
        //only LoggedIn user can access
        if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
        $this->load->library(array('form_validation'));
		
        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        $user = $this->ion_auth->user()->row();
        $is_error = false;
        $redirect = "";
        
		if ($this->form_validation->run() == false)
		{
			// display the form
			// set the flash data error message if there is one
			$message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
		}else{
            $identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
                $is_error = false;
                $message =  $this->ion_auth->messages();
                $redirect = site_url('auth/logout');
                
			}
			else
			{
				$is_error = true;
                $message =  $this->ion_auth->errors();
			}
        }
        $result = array("error"=>$is_error,"message" => $message, "redirect" => $redirect);
        echo json_encode($result);

    }
    // change password
	function change_password(){
		//only LoggedIn user can access
        if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
        $this->load->helper('form');
        $user = $this->ion_auth->user()->row();
        
        $this->viewparams['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
		$this->viewparams['old_password'] = array(
            'name' => 'old',
            'id'   => 'old',
            'type' => 'password',
            'class'	=> "form-control input-xlarge"
        );
        $this->viewparams['new_password'] = array(
            'name'    => 'new',
            'id'      => 'new',
            'type'    => 'password',
            'class'	=> "form-control input-xlarge",

            'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
        );
        $this->viewparams['new_password_confirm'] = array(
            'name'    => 'new_confirm',
            'id'      => 'new_confirm',
            'type'    => 'password',
            'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
            'class'	=> "form-control input-xlarge"

        );

        $this->viewparams['title_page']	= lang('lchange_password');
		parent::viewpage("vuser_changepass");

	}
    
	function test_sendmail(){
		/** sending email notification **/
		$this->load->library('email');
		
		$mail_config = $this->settings;
		
		$email = "dedy.adhiewirawan@gmail.com";
		$email_message= "testing email ";
		$this->email->set_newline("\r\n");
		$this->email->to($email);
		$this->email->from($mail_config['mail_activation_confirmation_sender_mail'], $mail_config['mail_activation_confirmation_sender_name']);
					 
	    $this->email->subject($mail_config['mail_activation_confirmation_subject']);
		$this->email->message($email_message);
    	$this->email->send();
    	
		echo $this->email->print_debugger();	
	}
	
	function generate_password($password,$salt){
		echo md5($password.$salt);
	}

}
