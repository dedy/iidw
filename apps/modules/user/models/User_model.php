<?php
class User_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
        Administrators can edit all projects.
        Project Managers can edit the projects they created.
    */
    function canEditProject($project){
        if(isAdmin()){
            return true;
        }else{
            if(isManager()){
                if($project->project_manager_id == _UserId()){
                    return true;
                }
            }
        }

        return false;
    }

    
    //set locked by email 
    //used if login too many attemps 
    function setLocked($email){
        $data["updated_on"] = date("Y-m-d H:i:s");
        $data["updated_by"] = _userid();
        $data['is_locked']  = '1';
        $this->db->where('email', $email);
        $this->db->update('users', $data);
    }

    //set locked / unlocked
    //used in User Administration
    function set_locked($id,$data){
        $data["updated_on"]	= date("Y-m-d H:i:s");
        $data["updated_by"]	= _userid();
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    /* countSearchUser */
    function count_search($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
	function get_user_data($id){
		$searchval['id'] = $id;
		$result = $this->search(1,1,'full_name','asc',$searchval);
		return $result[0];
	}

    public function getProjectManager()
    {
        
        $result = $this->search(0, 0, 'full_name', 'asc');
        return $result;
    }

    function get_row($params = array()){
        $this->db->select("id");
        $this->db->select("id as user_id");
        $this->db->select("email");
        $this->db->select("username");
        $this->db->select("active");
        $this->db->select("is_locked");
        $this->db->select("last_login");

        $this->db->where('users.username',$params['username']);
        $this->db->from('users')->order_by('id', 'desc');

        $query = $this->db->get();
        $result = $query->result();
     
        if($result) {
            return $result[0];
        } else {
            return false;
        }
    }
	
   	/* delete user*/
    function delete($data){
       	//-- delete records    	
    	$this->db->where('id', $data['id']);
		$this->db->delete('users'); 
    }
    
    /* insert_users */
    function insert($values){
		$this->db->insert("users", $values); 
		return $this->db->insert_id();
	}
	
	function update($data,$id){
		$data['updated_by'] = _userid();
		$data['updated_on'] = time();
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
	}

	function search($limit=0,$page=0,$sidx='',$sord='',$params=array()){
    	
        if(isset($params['id']) && $params['id']){
            $this->db->where('users.id',$params['id']);
        }

        if(isset($params['username'])) {
            $this->db->like('users.username',$params['username']);
        }

        if(isset($params['full_name']) && $params['full_name'] ){
            $this->db->like('users.full_name',$params['full_name']);
        }

        if(isset($params['group_id']) && $params['group_id'] ){
            $this->db->like('users_groups.group_id',$params['group_id']);
        }

        if(isset($params['active'])){
            if($params['active'] <> '-1'){
                $this->db->where('users.active',$params['active']);
            }
        }

        if(isset($params['locked'])){
            if($params['locked'] <> '-1'){
                $this->db->where('users.is_locked',$params['locked']);
            }
        }

        if(isset($params['include_id']) && $params['include_id'] ){
            if(is_array($params['include_id'])){
                $this->db->or_where_in('users.id',$params['include_id']);

            }else{
                $this->db->or_where('users.id',$params['include_id']);
            }
        }


		if($this->is_count){
			$this->db->select("count(users.id) as total");
		}else{
			$this->db->select("users.*");
			$this->db->select("users.id as user_id");
            $this->db->select("groups.name as user_group_name");
			$this->db->select("groups.description as user_group_description");
			$this->db->select("groups.id as group_id");
            
			$this->db->select("FROM_UNIXTIME(users.last_login ,'%d/%m/%Y %H:%i:%s') as last_login_fmt",false);
			$this->db->select("FROM_UNIXTIME(users.created_on ,'%d/%m/%Y %H:%i:%s') as created_on_fmt",false);
			$this->db->select("FROM_UNIXTIME(users.updated_on ,'%d/%m/%Y %H:%i:%s') as updated_on_fmt",false);

			$this->db->select("a.username as created_by_username");
			$this->db->select("b.username as updated_by_username");
       }

		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->join('users_groups',"users_groups.user_id = users.id","left outer");
		$this->db->join("groups","groups.id = users_groups.group_id");
    	$this->db->from('users');
		$this->db->join('users a','a.id = users.created_by','left outer');
		$this->db->join('users b','b.id = users.updated_by','left outer');
		$query = $this->db->get();
 		return $query->result();
    }
    
    function setActive($id,$data){
		$data["updated_on"]	= date("Y-m-d H:i:s");
		$data["updated_by"]	= _userid();
	
  		$this->db->where('id', $id);
		$this->db->update('users', $data); 
	}

    function getUserGroup(){
       // $this->db->where("id >=",_groupid());
		$this->db->order_by("id","asc");
        $this->db->from('groups');
		
		$query = $this->db->get();
        
 		return $query->result();
    }
    
    function search_simple($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('users.id',$searchval['id']);
        }

        if(isset($searchval['country_id']) && $searchval['country_id']){
            $this->db->where('users.country_id',$searchval['country_id']);
        }
        
        if(isset($searchval['username'])){
            $this->db->where('users.username',$searchval['username']);
        }
        if(isset($searchval['exclude_id']) && $searchval['exclude_id']){
            $this->db->where('users.id <>',$searchval['exclude_id']);
        }
        
		if($this->is_count){
			$this->db->select("count(users.id) as total");
		}else{
			$this->db->select("users.username");
			$this->db->select("users.id as user_id");
			$this->db->select("country.name as country_name");
			$this->db->select("country.id as country_id");
            
        }
        
          //exclude admin
        //$this->db->where("user_id <> ",'1');
        		
        
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->join('users_groups',"users_groups.user_id = users.id","left outer");
		$this->db->join("groups","groups.id = users_groups.group_id");
		$this->db->join("country","country.id = users.country_id","left outer");
        $this->db->from('users');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    function username_exists($username,$id){
        $this->db->where("username",$username);
        if($id){
            $this->db->where("id <>",$id);
        }
        $this->db->from('users');
		
		$result = $this->db->get()->result();
        
        if(count($result)){
            return true;
        } else {
            return false;
        }
    }
    
    function is_used($id){
        //check in table files
        $this->db->select("count(id) as total");
        $this->db->where("created_by",$id);
		$query = $this->db->get('files');
		$result = $query->result();
        
        if($result[0]->total > 0){
            return true;
        }
        
        return false;
    }
}